<h2>HICO API App</h2>

<h3>About The App:</h3>
<ul>
    <li>Data persistance is done with SQLite</li>
    <li>Supertest and Jest are used on the server side to test api endpoints</li>
    <li>Server runs on port 3000 and client on 3001</li>
    <li>Tested with Node 16 and React 18.</li>
    <li>I've setup a Jenkins deployment on my local machine.</li>
</ul>
