import Container from 'react-bootstrap/Container';
import React, { useState } from 'react';
import {Row, Col, Card, Button, Collapse} from 'react-bootstrap';
import Table from "./components/table.js";
import EmployeeForm from "./components/form/form.js";

function App() {
  const [employeeDetails, setEmployeeDetails] = useState();
  const [dataUpdate, setDataUpdate] = useState();
  const [showEmployeeForm, setShowEmployeeForm] = useState(false);

  return (
    <>
      <Container >
          <Row className='m-3'>
            <Col>
                <div style={{ textAlign: 'center' }}>Current Employees</div>
                <Button className="float-end" 
                        variant={showEmployeeForm ? "danger" : "primary"} 
                        size="sm" 
                        onClick={() => setShowEmployeeForm(!showEmployeeForm)}
                        aria-controls="employee-form"
                        aria-expanded={showEmployeeForm}>
                    {showEmployeeForm ? "Cancel New Employee" : "Add Employee"}
                </Button>
              </Col>
          </Row>
          <Row>
          <Col>
            <Card className='mb-4'>
              {/* 
                If a user clicks on a table row, the "add user" button should go back to the original state
                and form fields should be cleared in case the user is editing an existing user.
                Table row data should also be passed to the form when row is clicked to be edited
              */}
              <Table editEmployee={details => {setEmployeeDetails(details); setShowEmployeeForm()}} 
                      updateTable={dataUpdate} />
            </Card>
            <Collapse in={showEmployeeForm || employeeDetails?.id ? true : false}>
              <Card id="employee-form">
                <EmployeeForm addEmployee={showEmployeeForm} 
                              getEmployeeForEditing={employeeDetails}
                              employeeDataUpdate={data => setDataUpdate(data)} />
              </Card>
            </Collapse>
          </Col>
        </Row>
      </Container>
    </>
  );
}

export default App;

