import React, {useState, useRef, useEffect, useMemo} from 'react';
import {Form, Container, Button, Col, Row, Card, Alert} from 'react-bootstrap';
import Input from './inputs';
import EmployeeActionsService from '../../services/EmployeeActionsService'
import HandleApiService from '../../services/HandleApiService'
import FormHelper from '../../helpers/FormHelpers'

const EmployeeForm = ({addEmployee, getEmployeeForEditing, employeeDataUpdate}) => {

    const helpers = new FormHelper()
    const [formFields, setFormFields] = useState(helpers.emptyStateFields());
    const [message, setMessage] = useState({type:"", message:""})

    // Store multiple form values so they can be sent via api as an object.
    const storeFormInputs = event => {
        setFormFields(
            values => ({...values, [event.target.name]: event.target.value})
        )
    }

    const formatNumberWithSpaces = val => {
        storeFormInputs(val)

        let newInteger = val.target.value.replaceAll(' ', '')
        val.target.value = Number(newInteger).toLocaleString().replaceAll(',', ' ');
    }

    const form = useRef();
    const clearFormInputs = () => {
        form.current.reset()
        setFormFields(helpers.emptyStateFields());
    }

    //Populate the form fields when a table row is clicked.
    useEffect(() => {
        setFormFields({...getEmployeeForEditing})
    },[getEmployeeForEditing])

    // This will clear the form if the "Add Employee" button is clicked
    // It also prevents the previous form data from being populated 
    // when the "Cancel Adding Employee" button is clicked
    useMemo(() => {
        setFormFields({})
    },[addEmployee])

    // Send form data to server
    const saveEmployeeData = async () => {
        // Validate form inputs
        if(!formFields.first_name || !formFields.last_name || !formFields.salutation) {
            setMessage({
                type:"danger",
                message:`Please fill in all the required fields market with *`
            })
            return;
        }

        const action = addEmployee ? "add" : "edit"

        try{
            const employeeService = new EmployeeActionsService(HandleApiService)
            const url = `/payroll/employeeinformation/${action}_employee`;
            const resp = await employeeService.editEmployee(url, formFields)
    
            if(resp.success === "true"){
                setMessage({
                    type:"success",
                    message:`${formFields.first_name} ${formFields.last_name} 
                            ${addEmployee ? "added" : "updated"} successfully.`
                })
    
                employeeDataUpdate(true)
                clearFormInputs()
            }
        } catch(error) { 
            console.log(error) 
            setMessage({
                type:"danger",
                message:`Could not ${action} ${formFields.first_name} ${formFields.last_name}'s profile`
            })
        }
    }

	return (
        <Card>
            <Container>
                <Form ref={form} method='POST'>
                    <Row className='m-4 '>
                        <Col md="12">
                            <div style={{ textAlign: 'center' }}>
                                {addEmployee ? "Add New" : "Edit"} Employee Information
                            </div>
                        </Col>
                    </Row>
                    {message.message && 
                        <Alert key={message.type} variant={message.type} onClose={() => setMessage({})} dismissible>
                            {message.message}
                        </Alert>
                    }
                    <Row className='m-2'>
                        <Col className='mb-3'>
                            <Button className="float-end" 
                                    variant="primary" 
                                    size="sm"
                                    onClick={saveEmployeeData}>
                                Save
                            </Button>
                            <Button className="float-end" 
                                    style={{marginRight: '1em'}} 
                                    variant="danger" 
                                    size="sm" 
                                    onClick={clearFormInputs}>
                                Reset Fields
                            </Button>
                        </Col>
                    </Row>
                    <Row>
                        <Input type="text" 
                                labelName="First Name(s) *" 
                                formName="first_name" 
                                storeFormInputs={storeFormInputs} 
                                inputValue={formFields.first_name} />

                        <Input type="text" 
                                labelName="Full Name" 
                                formName="full_name" 
                                disableInput={true}
                                inputValue={
                                    formFields.first_name && formFields.last_name 
                                    ? formFields.first_name + " " + formFields.last_name 
                                    : null
                                } />
                    </Row>
                    <Row>
                        <Input type="text" 
                                labelName="Last Name *" 
                                formName="last_name" 
                                storeFormInputs={storeFormInputs} 
                                inputValue={formFields.last_name} />

                        <Input type="text" 
                                labelName="Gross Salary $PY" 
                                formName="gross_salary" 
                                storeFormInputs={formatNumberWithSpaces}
                                inputValue={formFields.gross_salary} />
                    </Row>
                    <Row>
                        <Col>
                            <Form.Group as={Row} className="mb-3" controlId="formHorizontalName">
                                <Form.Label column sm={4}>Salutation *</Form.Label>
                                <Col sm={8}>
                                    <Form.Select name='salutation' onChange={storeFormInputs} required>
                                        <option>Please select your title</option>
                                        {
                                            Object.keys(helpers.salutationsTitleMap()).map( title => (
                                                <option key={title} value={title} selected={formFields.salutation === title}>
                                                    {title}
                                                </option>
                                            ))
                                        }
                                    </Form.Select>
                                </Col>
                            </Form.Group>
                        </Col>
                        <Col>
                            <Form.Group as={Row} className="mb-3" controlId="formHorizontalFullName" >
                                <Form.Label column sm={4}>Employee Profile Color</Form.Label>
                                <Col sm={8}>
                                    {
                                        ['green','blue','red','default'].map( color => (
                                            <Form.Check
                                                key={color}
                                                inline
                                                label={helpers.capitalizeFirstLetter(color)}
                                                name="profile_color"
                                                type="radio"
                                                onChange={storeFormInputs}
                                                value={color}
                                                checked={formFields.profile_color === color}
                                            />
                                        ))
                                    }
                                </Col>
                            </Form.Group>
                        </Col>
                    </Row>
                    <Row>
                        <Col>
                            <Form.Group as={Row} className="mb-3" controlId="formHorizontalName">
                                <Form.Label column sm={4}>Gender</Form.Label>
                                <Col sm={8}>
                                    {
                                        ['male','female','unspecified'].map( gender => (
                                            <Form.Check
                                                key={gender}
                                                inline
                                                label={helpers.capitalizeFirstLetter(gender)}
                                                name="gender"
                                                type="radio"
                                                onChange={storeFormInputs}
                                                value={gender}
                                                checked={helpers.salutationsTitleMap()[formFields.salutation] 
                                                            === helpers.capitalizeFirstLetter(gender)}
                                                readOnly
                                            />
                                        ))
                                    }
                                </Col>
                            </Form.Group>
                        </Col>
                        <Col></Col>
                    </Row>
                    <Row>
                        <Input type="number" 
                                labelName="Employee #" 
                                formName="employee_number" 
                                storeFormInputs={storeFormInputs} 
                                inputValue={formFields.employee_number} />
                        <Col></Col>
                    </Row>
                </Form>
            </Container>
        </Card>
	);
};

export default EmployeeForm;