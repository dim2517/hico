import React from 'react';
import {Form, Col, Row} from 'react-bootstrap';

const Input = props => {
    return (
        <Col>
            <Form.Group as={Row} className="mb-3" controlId="formHorizontalName">
                <Form.Label column sm={4}>{props.labelName}</Form.Label>
                <Col sm={8}>
                    <Form.Control type={props.type} 
                                    name={props.formName} 
                                    onChange={props.storeFormInputs} 
                                    required={props.requiredInput != null}
                                    disabled={props.disableInput != null}
                                    defaultValue={props.inputValue ? props.inputValue : ""}
                    />
                </Col>
            </Form.Group>
        </Col>
    )
}

export default Input

