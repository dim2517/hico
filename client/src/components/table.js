import React, { useState, useMemo } from "react";
import DataTable from "react-data-table-component";
import EmployeeActionsService from '../services/EmployeeActionsService'
import HandleApiService from '../services/HandleApiService'

const tableColumns = [
  { name: "id", selector: row => row.id, sortable:true, omit: true},
  { name: "Employee #", selector: row => row.employee_number,sortable:true },
  { name: "First Name", selector: row => row.first_name,sortable:true },
  { name: "Last Name", selector: row => row.last_name,sortable:true },
  { name: "Salutation", selector: row => row.salutation,sortable:true },
  { name: "Profile Color", selector: row => row.profile_color,sortable:true },
  { name: "Gross Salary", selector: row => row.gross_salary, omit: true },
];

const Table = (props) => {
    const [employees, setEmployees] = useState([]);

    const employeeService = new EmployeeActionsService(HandleApiService)

    const fetchEmployees = async () => {
      try{
        const url = "/payroll/employeeinformation/fetch_all_employees"

        setEmployees( await employeeService.getDataForAllEmployees(url))
      } catch(error) { 
        console.log(error) 
      }
    }

    useMemo(() => {
        fetchEmployees()
    }, [props.updateTable]);

    // Set row background based on color passed via json
    const conditionalRowStyles = [
      {
        when: (row) => true,
        style: (row) => ({
          color: row.profile_color !== "default" ? "white" : "black",
          backgroundColor: row.profile_color,
        }),
      },
    ];

    // Map table row data according to json returned from api
    let tableRows = employees?.map((result) => {
      return {
        id: result.id,
        employee_number: result.employee_number,
        first_name: result.first_name,
        last_name: result.last_name,
        salutation: result.salutation,
        profile_color: result.profile_color,
        gross_salary: result.gross_salary,
      };
    });

    return (
        <>
            <DataTable
              columns={tableColumns}
              data={tableRows}
              onSelectedRowsChange={selectedRows => props.editEmployee(selectedRows)}
              onRowClicked={selectedRows => props.editEmployee(selectedRows)}
              conditionalRowStyles={conditionalRowStyles}
              pagination
              defaultSortAsc={false}
              defaultSortFieldId={1}
            />
        </>
    );
};

export default Table