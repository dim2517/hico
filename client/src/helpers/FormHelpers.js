class FormHelpers{
    capitalizeFirstLetter (string){
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    salutationsTitleMap(){
        return {
            Dr:"",
            Mr:"Male",
            Ms:"Female",
            Mrs:"Female",
            Mx:"Unspecified"
        }
    }

    emptyStateFields(){
        return {
            first_name: "",
            last_name: "",
            gross_salary: "",
            salutation: "",
            profile_color: "",
            gender: "",
            employee_number: ""
        }
    }
}



export default FormHelpers