// A reusable CRUD class to handle
// employee data manipulation.

class EmployeeActionsService {

    constructor(httpClient){
        this.httpClient = httpClient
    }

    async getDataForAllEmployees(url){
        try{
            return (await this.httpClient.get(url)).json()
        }catch(error) { 
            console.log(error) 
        }
    }

    async editEmployee(url, data){
        try{
            console.log(data)
            return (await this.httpClient.post(url, data)).json()
        }catch(error){
            console.log(error)
        }
    }
}

export default EmployeeActionsService