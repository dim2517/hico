const HandleApiService = {
    async get(url){
        return await fetch(process.env.REACT_APP_SERVER_URL + url)
    },

    async post(url, body){
        return await fetch (
            process.env.REACT_APP_SERVER_URL + url,
            {
                method: "POST",
                body: JSON.stringify(body),
                headers: {
                    'Content-Type':'application/json'
                }
            }
        )
    }
}

export default HandleApiService