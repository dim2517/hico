const app = require('../app')
const request = require('supertest')

// Generate some fake stuff for testing
const random_number = Math.floor(Math.random() * 100000) + 1000;
const randomString = len => Math.random().toString(36).substring(2,len+2);
const fakeNames = randomString(9)

describe('add employee data', () => {
    it('the api should return a 200 status code and json success message', async () => {
       return await request(app)
            .post('/payroll/employeeinformation/add_employee')
            .send({
                    "employee_number": random_number,
                    "first_name": fakeNames,
                    "last_name": fakeNames,
                    "salutation": "Mr",
                    "profile_color": "green",
                    "gross_salary": "888 412"
                })
            .expect(200, {"success":"true"});
    })
})

describe('fetch all employee data', () => {
    it('the api should return a 200 status code and json data', async () => {
       await request(app)
            .get('/payroll/employeeinformation/fetch_all_employees')
            .expect(200)
            .expect('Content-Type', 'application/json; charset=utf-8');
    })
})

describe('edit employee data', () => {
    it('the api should return a 200 status code and json success message', async () => {
       return await request(app)
            .post('/payroll/employeeinformation/edit_employee')
            .send({
                    "employee_number": random_number,
                    "first_name": "John",
                    "last_name": "Mahoney",
                    "salutation": "Mr",
                    "profile_color": "green",
                    "gross_salary": "900 000"
                })
            .expect(200, {"success":"true"});
    })
})

