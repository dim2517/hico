const Database = require('../data/db');

module.exports.EmployeesAPI = app => {
    app.get('/payroll/employeeinformation/fetch_all_employees', async (request, response) => {
        let _employeesDB = new Database();

        
        response.json(await _employeesDB.fetchAllEmployees().then(
            employeeData => employeeData
        ));

        _employeesDB.close();
    });

    app.post('/payroll/employeeinformation/add_employee', async (request, response) => {
        let _employeesDB = new Database();

        response.json(await _employeesDB.addEmployee(request.body).then(
            employeeData => employeeData
        ))

        _employeesDB.close();
    });

    app.post('/payroll/employeeinformation/edit_employee', async (request, response) => {
        let _employeesDB = new Database();

        response.json(await _employeesDB.editEmployee(request.body).then(
            employeeData => employeeData
        ))

        _employeesDB.close();
    });
}
