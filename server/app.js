const express = require('express');
const {EmployeesAPI} = require('./api/project');
const cors = require('cors');
const bodyParser = require('body-parser');

const app = express();
app.use(cors());
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());

EmployeesAPI(app);

module.exports = app