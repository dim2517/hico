const sqlite3 = require('sqlite3').verbose();

module.exports = class Database {
    constructor() {
      const path = require('path');
      const dbPath = path.resolve(__dirname, 'hico_db.sqlite');
    
      this.db = new sqlite3.Database(dbPath, sqlite3.OPEN_READWRITE, (err) => {
        if (err) console.error(err.message);
      });
    }

    // select all data from table employees 
    async fetchAllEmployees() {
        return await new Promise(resolve => {
            this.db.all("SELECT * FROM employees", (err, rows) => {
                resolve(rows);
            }); 
        });
    }

    // insert data into table employees
    async addEmployee(data) {
        return await new Promise(resolve => {
            let fullName = data.first_name && data.last_name ? data.first_name + " " + data.last_name : null

            // Add employee record with a prepared statement
            this.db.all(`INSERT INTO employees (employee_number,first_name,full_name,last_name,salutation,profile_color,gross_salary) 
                        VALUES (?, ?, ?, ?, ?, ?, ?)`,
                        [
                            data.employee_number || null, 
                            data.first_name || null, 
                            fullName, 
                            data.last_name || null, 
                            data.salutation || null,
                            data.profile_color || null, 
                            data.gross_salary || null
                        ],
                        (error, row) => {
                            if(!error) resolve({"success": "true"})
                            console.log(error)
                        });
        });
    }

    // insert data into table employees
    async editEmployee(data) {
        return await new Promise(resolve => {
            let fullName = data.first_name && data.last_name ? data.first_name + " " + data.last_name : null

            this.db.all(`UPDATE employees 
                        SET employee_number = ?,
                        first_name = ?,
                        full_name = ?,
                        last_name = ?,
                        salutation = ?,
                        profile_color = ?,
                        gross_salary = ?
                        WHERE id = ? OR employee_number = ?`,
                        [
                            data.employee_number || null, 
                            data.first_name || null, 
                            fullName, 
                            data.last_name || null, 
                            data.salutation || null,
                            data.profile_color || null, 
                            data.gross_salary || null,
                            data.id || null,
                            data.employee_number || null
                        ],
                        (error, row) => {
                            if(!error) resolve({"success": "true"})
                            console.log(error)
                        });
        });
    }

    // close database connection
    close() {
        this.db.close();
    }
}

