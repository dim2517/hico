const app = require('./app');
const {ENV_VARS} = require('./config');

app.listen(ENV_VARS.PORT, () => {
    console.log(`Listening on port: ${ENV_VARS.PORT}`);
}).on('error', (err) => {
    console.log(err);
    process.exit();
})